(* Copyright (c) 2020, 2021, Guillaume Munch-Maccagnoni & INRIA
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*)

let callback _ = None

let tracker = { Stdlib.Gc.Memprof.null_tracker with
                alloc_minor = callback ;
                alloc_major = callback }

let sampling_rate = 0.01

let () = Memprof_limits.Memprof.start ~sampling_rate tracker
