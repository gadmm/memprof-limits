#!/bin/bash

# Pregen svg assets
# Copy svg assets into the assets dir after converting text to path
#
# This requires inkscape and local fonts. Pregenerated assets must
# therefore be checked into the repo and included with the package.
DEST_DIR=$PWD
ORIG_DIR=$PWD/../doc/svg/

cd $ORIG_DIR || { echo "failed cd"; exit 1; }

for img in *.svg; do
    echo "Flattening $img..."

    # Initialize the retry count
    retry_count=0
    max_retries=10
    inkscape_success=0

    # Retry loop for Inkscape command because it CRASHES
    while [ $retry_count -lt $max_retries ] && [ $inkscape_success -eq 0 ]; do
        # Convert text to paths in the SVG, preserving the original SVG
        if inkscape "$img" --export-type=svg --export-text-to-path --export-filename="$DEST_DIR/$img"; then
            inkscape_success=1
            echo "Inkscape processing succeeded."
        else
            retry_count=$((retry_count+1))
            echo "Inkscape processing failed, retrying $retry_count/$max_retries..."
            sleep 1  # sleep for a second before retrying
        fi
    done

    if [ $inkscape_success -eq 1 ]; then
        echo "Copied $img to assets"
    else
        echo "Inkscape processing failed after $max_retries attempts, skipping $img."
    fi
done
