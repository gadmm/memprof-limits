.PHONY: doc clean build test doc-pregen package

build:
	dune build

test:
	dune test

DOCS=_build/default/_doc/_html/memprof-limits/

doc:
	dune build @doc
	@mkdir -p $(DOCS)/assets/
	@cp assets/*.svg $(DOCS)/assets/
	@echo Read the documentation at $(DOCS)/index.html

doc-pregen:
	@cd assets && ./incorporate-assets.sh

package: doc-pregen

clean:
	dune clean
	rm -rf _doc
