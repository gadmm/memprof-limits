(* Copyright (c) 2020, 2021, Guillaume Munch-Maccagnoni & INRIA
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*)

(** A thread-local store *)
type 'a t
val create : unit -> 'a t
val get : 'a t -> 'a option
val set : 'a t -> 'a option -> unit
val clear : 'a t -> unit
