(* Copyright (c) 2020, 2021, Guillaume Munch-Maccagnoni & INRIA
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*)

type t = bool Atomic.t
let create () = Atomic.make false
let set x = Atomic.set x true
let is_set x = Atomic.get x
