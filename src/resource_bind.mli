(* Copyright (c) 2020, 2021, Guillaume Munch-Maccagnoni & INRIA
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*)

(** Open {!Memprof_limits.Resource_bind} to enable the [let&] binder
    for resources. *)

val ( let& ) : (scope:('a -> 'b) -> 'b) -> ('a -> 'b) -> 'b
(** RAII-style notation for resources cleaned-up at the end of
    scope. Example:
    {[open Memprof_limits.Resource_bind

    let with_my_resource x =
      Memprof_limits.Masking.with_resource ~acquire x ~release

    let f x =
      let& resource = with_my_resource x in
      …]} *)
