(* Copyright (c) 2020, 2021, Guillaume Munch-Maccagnoni & INRIA
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*)

include Stdlib.Gc.Memprof
let start = Memprof_server.start_1
let stop = Memprof_server.stop_1
