(* Copyright (c) 2020, 2021, Guillaume Munch-Maccagnoni & INRIA
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*)

(** Use this reimplementation of the Memprof interface if you need to
    profile a program that uses Memprof-limits.

    Note: the expectancy (1/[sampling_rate]) provided in
    [Memprof.start] is rounded to the nearest integer for the accuracy
    of allocation limits accounting. *)

include module type of Stdlib.Gc.Memprof
